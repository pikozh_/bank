package model;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Bank implements Runnable {

    public static int balance = 50000;
    Lock lock = new ReentrantLock();
    Random random = new Random();

    public void run() {
        lock.lock();
        try {
            int choice = random.nextInt(2);
            if (choice == 0) {
                doCredit();
            } else {
                doDeposit();
            }
            lock.unlock();
            TimeUnit.MILLISECONDS.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
    private void doCredit () {
        balance += 100;
        System.out.println(Thread.currentThread().getName()+ " CREDIT " + balance);
    }

    private void doDeposit(){
        balance -= 100;
        System.out.println(Thread.currentThread().getName()+ " DEPOSIT " + balance);
    }
}
