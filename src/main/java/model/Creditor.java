package model;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Creditor implements Runnable {
    Bank resource;
    Lock lock = new ReentrantLock();
    public void run() {
        Bank.balance = Bank.balance + 100;
        System.out.println(Thread.currentThread().getName() + "CREDITOR balance=" + resource.balance);
    }
}
