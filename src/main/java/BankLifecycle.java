import model.Bank;
import model.Creditor;
import model.Depositor;

import java.util.Random;

public class BankLifecycle {
    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            Thread thread = new Thread(new Bank());
            thread.setName("Client " + i + " ");
            thread.start();
        }
    }
}
